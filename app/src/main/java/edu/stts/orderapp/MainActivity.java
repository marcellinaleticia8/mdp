package edu.stts.orderapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.VectorEnabledTintResources;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private EditText edName;
    private RadioGroup rgType;
    private RadioButton rbTea, rbCoffee, rbSmoothies;
    private CheckBox cbPearl, cbPudding, cbRedBean, cbCoconut;
    private Button btnMinus, btnPlus, btnAdd, btnDelete, btnReset;
    private TextView txtQty, txtTotal, txtName;
    private RecyclerView rvOrder;
    private OrderAdapter adapter;
    private ArrayList<Order> arrOrder = new ArrayList<>();
    private long total = 0;
    private int index = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edName = findViewById(R.id.edName);
        rgType = findViewById(R.id.radioGroup);
        rbTea = findViewById(R.id.rb_Tea);
        rbCoffee = findViewById(R.id.rb_Coffee);
        rbSmoothies = findViewById(R.id.rb_Smoothies);
        cbPearl = findViewById(R.id.cb_Pearl);
        cbPudding = findViewById(R.id.cb_Pudding);
        cbRedBean = findViewById(R.id.cb_RedBean);
        cbCoconut = findViewById(R.id.cb_Coconut);
        btnMinus = findViewById(R.id.bt_Minus);
        btnPlus = findViewById(R.id.bt_Plus);
        btnAdd = findViewById(R.id.bt_Add);
        btnDelete = findViewById(R.id.bt_Delete);
        btnReset = findViewById(R.id.bt_Reset);
        txtQty = findViewById(R.id.tx_Qty);
        txtTotal = findViewById(R.id.tx_Total);
        txtName = findViewById(R.id.tx_Nama);
        rvOrder = findViewById(R.id.rv_Menu);
        adapter = new OrderAdapter(arrOrder, new RVClickListener() {
            @Override
            public void recyclerViewClick(View v, int pos) {
                index = pos;
                resetInput();

                String type = arrOrder.get(index).getType();
                if(type.contentEquals("Tea")) { rbTea.setChecked(true); }
                else if(type.contentEquals("Coffee")) { rbCoffee.setChecked(true); }
                else if(type.contentEquals("Smoothies")) { rbSmoothies.setChecked(true); }

                ArrayList<String> topping = new ArrayList<>();
                topping = arrOrder.get(index).getToppings();
                for (String top : topping) {
                    if(top.contentEquals("Pearl")) { cbPearl.setChecked(true); }
                    else if(top.contentEquals("Pudding")) { cbPudding.setChecked(true); }
                    else if(top.contentEquals("Red Bean")) { cbRedBean.setChecked(true); }
                    else if(top.contentEquals("Coconut")) { cbCoconut.setChecked(true); }
                }

                txtQty.setText(arrOrder.get(index).getQty() + "");
            }
        });
        RecyclerView.LayoutManager lm = new LinearLayoutManager(MainActivity.this);
        rvOrder.setLayoutManager(lm);
        rvOrder.setAdapter(adapter);
        total = 0;

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(txtQty.getText().toString());
                if(qty>1){ txtQty.setText(String.valueOf(qty-1)); }
            }
        });

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(txtQty.getText().toString());
                txtQty.setText(String.valueOf(qty+1));
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edName.getText().toString().contentEquals("")){
                    txtName.setText("Hi, " + edName.getText().toString() +" !");

                    String type="";
                    int qty = Integer.parseInt(txtQty.getText().toString());
                    int subtotal = 0;
                    if(rbTea.isChecked()) {
                        type = "Tea";
                        subtotal = qty * 23000;
                    }else if(rbCoffee.isChecked()) {
                        type = "Coffee";
                        subtotal = qty * 25000;
                    }else if(rbSmoothies.isChecked()) {
                        type = "Smoothies";
                        subtotal = qty * 30000;
                    }

                    ArrayList<String> arrToppings = new ArrayList<>();
                    if(cbPearl.isChecked()){
                        arrToppings.add("Pearl");
                        subtotal += 3000;
                    }
                    if(cbPudding.isChecked()){
                        arrToppings.add("Pudding");
                        subtotal += 3000;
                    }
                    if(cbRedBean.isChecked()){
                        arrToppings.add("Red Bean");
                        subtotal += 4000;
                    }
                    if(cbCoconut.isChecked()){
                        arrToppings.add("Coconut");
                        subtotal += 4000;
                    }

                    total += subtotal;
                    txtTotal.setText(total+"");

                    arrOrder.add(new Order(type, arrToppings, qty, subtotal));
                    adapter.notifyDataSetChanged();
                    resetInput(); index = -1;
                }else{
                    Toast.makeText(getApplicationContext(), "Field name cannot be empty", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(index!=-1){
                    int subtotal = (int) arrOrder.get(index).getSubtotal();
                    total -= subtotal;
                    txtTotal.setText(total + "");

                    arrOrder.remove(index);
                    adapter.notifyDataSetChanged();
                    resetInput(); index = -1;
                }else{
                    Toast.makeText(getApplicationContext(), "No item selected", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetInput();
                resetKet();
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void resetInput(){
        txtQty.setText("1");
        rgType.clearCheck();
        rbTea.setChecked(true);
        cbPearl.setChecked(false);
        cbPudding.setChecked(false);
        cbRedBean.setChecked(false);
        cbCoconut.setChecked(false);
    }

    public void resetKet(){
        edName.setText("");
        txtName.setText("Hi,Cust !");
        txtTotal.setText("0");
        arrOrder.clear();
        index = -1;
        total = 0;
    }
}
