package edu.stts.orderapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    private ArrayList<Order> orderList;
    private static RVClickListener myListener;

    public OrderAdapter(ArrayList<Order> orderList, RVClickListener rvcl) {
        this.orderList = orderList;
        myListener = rvcl;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.row_item_order, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        String top = "with Toppings: ";
        ArrayList<String> toppings = orderList.get(i).getToppings();
        for (String t : toppings){
            top += t + ", ";
        } top = top.substring(0, top.length()-2);

        viewHolder.tvQtyType.setText(orderList.get(i).getQty() + " " + orderList.get(i).getType());
        viewHolder.tvToppings.setText(top);
        viewHolder.tvHarga.setText("Rp " + orderList.get(i).getSubtotal());
    }

    @Override
    public int getItemCount() {
        return (orderList!=null) ? orderList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvQtyType, tvToppings, tvHarga;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvQtyType = itemView.findViewById(R.id.txtQtyType);
            tvToppings = itemView.findViewById(R.id.txtToppings);
            tvHarga = itemView.findViewById(R.id.txtHargaTotal);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myListener.recyclerViewClick(v, ViewHolder.this.getLayoutPosition());
                }
            });
        }
    }
}
