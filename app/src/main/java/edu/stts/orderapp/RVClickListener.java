package edu.stts.orderapp;

import android.view.View;

public interface RVClickListener {
    public void recyclerViewClick(View v, int pos);
}
